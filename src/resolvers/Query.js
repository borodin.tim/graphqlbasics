const Query = {
    users(parent, args, { db }, info) {
        if (args.query) {
            return db.users.filter((user) => user.name.toLowerCase().includes(args.query.toLowerCase()));
        } else {
            return db.users;
        }
    },
    posts(parent, args, { db }, info) {
        if (!args.query) {
            return db.posts;
        }
        return db.posts.filter(post => {
            const isTitleMatch = post.title.toLowerCase().includes(args.query.toLowerCase());
            const isBodyMatch = post.body.toLowerCase().includes(args.query.toLowerCase());
            return isTitleMatch || isBodyMatch;
        });
    },
    me() {
        return {
            id: 'abc123',
            name: 'Harley',
            email: 'harley@mail.com'
        };
    },
    post() {
        return {
            id: 'xyz987',
            title: 'Post one',
            body: 'This is the body of post one',
            published: true
        };
    },
    comments(parent, args, { db }, info) {
        return db.comments;
    }
};

export default Query;