const Subscription = {
    comment: {
        subscribe(parent, { postId }, { db, pubsub }, info) {
            const post = db.posts.find(post => post.id === postId && post.published);
            if (!post) {
                throw new Error('Unable to find post to create a subscruiption');
            }
            return pubsub.asyncIterator(`comment ${postId}`);
        }
    },
    post: {
        subscribe(parent, args, { db, pubsub }, info) {
            // const user = db.users.find(user => user.id === userId);
            // if (!user) {
            //     throw new Error('User not found, unabel to subscribe');
            // }
            return pubsub.asyncIterator(`post`);
        }
    }
}

export default Subscription;